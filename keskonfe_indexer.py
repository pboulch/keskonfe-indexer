from python_graphql_client import GraphqlClient
import time
import jwt
import urllib.request
import urllib.parse
import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder
from urllib.parse import urlsplit
import os.path
import magic
import json 
import re


class KeskonfeIndexer: 
    query_create_event = """
        mutation createEvent($organizerActorId: ID!, $attributedToId: ID, $title: String!, $description: String!, $beginsOn: DateTime!, $endsOn: DateTime, $status: EventStatus, $visibility: EventVisibility, $joinOptions: EventJoinOptions, $draft: Boolean, $tags: [String], $picture: MediaInput, $onlineAddress: String, $phoneAddress: String, $category: EventCategory, $physicalAddress: AddressInput, $options: EventOptionsInput, $contacts: [Contact]) {  createEvent(    organizerActorId: $organizerActorId    attributedToId: $attributedToId    title: $title    description: $description    beginsOn: $beginsOn    endsOn: $endsOn    status: $status    visibility: $visibility    joinOptions: $joinOptions    draft: $draft    tags: $tags    picture: $picture    onlineAddress: $onlineAddress    phoneAddress: $phoneAddress    category: $category    physicalAddress: $physicalAddress    options: $options    contacts: $contacts  ) {    id    uuid    title    url    local    description    beginsOn    endsOn    status    visibility    joinOptions    draft    picture {      id      url      __typename    }    publishAt    category    onlineAddress    phoneAddress    physicalAddress {      description      street      locality      postalCode      region      country      geom      type      id      originId      __typename    }    attributedTo {      id      domain      name      url      preferredUsername      avatar {        id        url        __typename      }      __typename    }    organizerActor {      avatar {        id        url        __typename      }      preferredUsername      domain      name      url      id      __typename    }    contacts {      avatar {        id        url        __typename      }      preferredUsername      domain      name      url      id      __typename    }    participantStats {      going      notApproved      participant      __typename    }    tags {      id      slug      title      __typename    }    options {      maximumAttendeeCapacity      remainingAttendeeCapacity      showRemainingAttendeeCapacity      anonymousParticipation      showStartTime      showEndTime      offers {        price        priceCurrency        url        __typename      }      participationConditions {        title        content        url        __typename      }      attendees      program      commentModeration      showParticipationPrice      hideOrganizerWhenGroupEvent      __typename    }    __typename  }}
    """

    query_upload_file = """
        mutation uploadMedia($file: Upload!, $name: String!) {
        uploadMedia(file: $file, name: $name) { id }
      }
    """

    query_refresh_token = """
        mutation RefreshToken($rt: String!) {
            refreshToken(refreshToken: $rt) {
                accessToken
                refreshToken
            }
        }
    """

    query_get_events = """
        query LoggedUserUpcomingEvents($afterDateTime: DateTime, $beforeDateTime: DateTime, $page: Int, $limit: Int) {
            loggedUser {
                id
                participations(
                afterDatetime: $afterDateTime
                beforeDatetime: $beforeDateTime
                page: $page
                limit: $limit
                ) {
                total
                elements {
                    event {
                    id
                    uuid
                    url
                    title
                    beginsOn
                    onlineAddress
                    }
                    id
                    role
                }
                }
            }
        }
    """

    query_get_address = """
        query SearchAddress($query: String!, $locale: String, $type: AddressSearchType) {  searchAddress(query: $query, locale: $locale, type: $type) {    ...AdressFragment    __typename  }}fragment AdressFragment on Address {  id  description  geom  street  locality  postalCode  region  country  type  url  originId  timezone  pictureInfo {    url    author {      name      url      __typename    }    source {      name      url      __typename    }    __typename  }  __typename}
    """

    graphql_client = GraphqlClient(endpoint="https://events.keskonfe.app/api")
    access_token = ""
    refresh_token = ""
    organizer_actor_id = "0"
    existing_events = []

    def __init__(self):
        self.get_access_token()
        self.graphql_client = GraphqlClient(endpoint="https://events.keskonfe.app/api", headers={"Authorization": "Bearer "+self.access_token})

    def save_new_token(self, new_access_token, new_refresh_token):
        pass

    def read_access_token(self):
        pass

    def refresh_access_token(self):
        self.graphql_client = GraphqlClient(endpoint="https://events.keskonfe.app/api", headers={})
        result = self.graphql_client.execute(query=self.query_refresh_token, variables={"rt": self.refresh_token})
        if "data" in result and "refreshToken" in result["data"] and "accessToken" in result["data"]["refreshToken"] and "refreshToken" in result["data"]["refreshToken"]:
            self.save_new_token(new_access_token= result["data"]["refreshToken"]["accessToken"], new_refresh_token= result["data"]["refreshToken"]["refreshToken"])

    def get_access_token(self):
        self.read_access_token()
        token_payload = jwt.decode(self.access_token, options={"verify_signature": False})
        if token_payload["exp"] < time.time() + 120 :
            self.refresh_access_token()
            self.read_access_token()

    def get_events(self):
        self.existing_events = []
        response = self.graphql_client.execute(query=self.query_get_events, variables={"limit": 500})
        try:
            if "data" in response and "loggedUser" in response["data"] and "participations" in response["data"]["loggedUser"] and "elements" in response["data"]["loggedUser"]["participations"]:
                for element in response["data"]["loggedUser"]["participations"]["elements"]:
                    if "role" in element and element["role"] == "CREATOR" and "event" in element:
                        self.existing_events.append({"title": element["event"]["title"], "beginsOn": element["event"]["beginsOn"], "url": element["event"]["onlineAddress"]})
                    else:
                        print("Error: Has element but not added")
            else:
                print("Error : Get Existings Events return error")
        except Exception as e:
            print("Error: No events (exception)")
            print(e)

    def event_already_exist(self, title, beginsOn, url):
        for event in self.existing_events:
            if event["url"] == url.split("?")[0] or (event["title"] == title and event["beginsOn"] == beginsOn):
                return True
        print("KeskonfeIndexer | event_already_exist | Event "+title+" not exists")
        return False
    
    def upload_picture_from_url(self, url):
        try: 
            path = urlsplit(url).path
            extension = os.path.splitext(path)[-1] # e.g. ".jpg"
            urllib.request.urlretrieve(url, 'currentImg'+extension)
            mime = magic.Magic(mime=True)
            multipart_data = MultipartEncoder( fields= {
                # a file upload field
                'file': ('file1', open('currentImg'+extension, 'rb'), mime.from_file('currentImg'+extension)),
                # plain text fields
                'variables': "{\"name\": \"astronef"+extension+"\", \"file\": \"file\"}",
                'query': self.query_upload_file,
            })
            response = requests.post('https://events.keskonfe.app/api', data=multipart_data,
            headers={'Content-Type': multipart_data.content_type, 'Authorization': 'Bearer '+self.access_token})
            response = json.loads(response.text)
            return response["data"]["uploadMedia"]["id"]
        except Exception as e: 
            print("Upload picture error : ")
            print(e)
            return None

    def convert_datagouv_address(self, address_json, contextual_name = None):
        if "features" in address_json and len(address_json["features"]) > 0 and "properties" in address_json["features"][0]:
            description = address_json["features"][0]["properties"]["label"]
            if contextual_name != None:
                description = contextual_name
            street = ""
            if "street" in address_json["features"][0]["properties"]:
                street = address_json["features"][0]["properties"]["street"]
            elif "locality" in address_json["features"][0]["properties"]:
                street = address_json["features"][0]["properties"]["locality"]
            result = {
                "country": "FRANCE",
                "description": description,
                "geom": f"{address_json['features'][0]['geometry']['coordinates'][0]};{address_json['features'][0]['geometry']['coordinates'][1]}",
                "id": None,
                "locality": address_json["features"][0]["properties"]["city"],
                "postalCode": address_json["features"][0]["properties"]["postcode"],
                "region": address_json["features"][0]["properties"]["context"],
                "street": street,
                "timezone": "",
                "type": "",
                "url": None
            }
            return result
        else:
            print("KeskonfeIndexer | search_existing_address | Address not found from adresse.data.gouv.fr")

    def search_existing_address(self, address, geoloc):
        if geoloc is not None:
            print("KeskonfeIndexer | search_existing_address | Try to convert geoloc into address with adresse.data.gouv.fr")
            response = requests.get(f"https://api-adresse.data.gouv.fr/reverse?lat={geoloc['latitude'].replace(',','.')}&lon={geoloc['longitude'].replace(',','.')}")
            address_json = response.json()
            return self.convert_datagouv_address(address_json, contextual_name = address)
        
        response = self.graphql_client.execute(query=self.query_get_address, variables={"locale": "fr", "query": address})
        try:
            if "data" in response and "searchAddress" in response["data"]:
                if len(response["data"]["searchAddress"]) > 0 :
                    physical_address = response["data"]["searchAddress"][0]
                    result = {
                        "country": physical_address["country"],
                        "description": physical_address["description"],
                        "geom": physical_address["geom"],
                        "id": None,
                        "locality": physical_address["locality"],
                        "originId": physical_address["originId"],
                        "postalCode": physical_address["postalCode"],
                        "region": physical_address["region"],
                        "street": physical_address["street"],
                        "timezone": physical_address["timezone"],
                        "type": physical_address["type"],
                        "url": None
                    }
                    return result
                else: 
                    print("KeskonfeIndexer | search_existing_address | Try to get address from adresse.data.gouv.fr")
                    response = requests.get(f"https://api-adresse.data.gouv.fr/search/?q={urllib.parse.quote(address)}&type=&autocomplete=0")
                    address_json = response.json()
                    return self.convert_datagouv_address(address_json)
        except Exception as e:
            print(e)

    
    def get_create_event_variables(self, title, description, beginsOn, endsOn=None, location=None, onlineAddress=None):
        result = {
            "title": title,
            "description": description,
            "beginsOn": beginsOn,             
            "status": 'CONFIRMED',                       
            "visibility": 'PUBLIC',                      
            "joinOptions": 'FREE',                       
            "draft": False,                              
            "tags": [],                                  
            "onlineAddress": onlineAddress,                         
            "phoneAddress": '',
            "physicalAddress": location,
            "options": {                                 
                "maximumAttendeeCapacity": 0,              
                "remainingAttendeeCapacity": 0,            
                "showRemainingAttendeeCapacity": False,    
                "anonymousParticipation": True,           
                "hideOrganizerWhenGroupEvent": False,      
                "offers": [],                              
                "participationConditions": [],             
                "attendees": [],                           
                "program": '',                             
                "commentModeration": 'CLOSED',          
                "showParticipationPrice": False,           
                "showStartTime": True,                     
                "showEndTime": True                        
            },                                         
            "attributedToId": None,                      
            "contacts": [],                              
            "organizerActorId": self.organizer_actor_id
        }
        if endsOn is not None:
            result["endsOn"] = endsOn
        return result
    
    def get_physical_address(self):
        pass

    def get_organizer_actor_id(self):
        pass

    def create_event(self, picture_id, title, description, beginsOn, endsOn=None, location=None, url=None):
        try:
            params = self.get_create_event_variables(title=title, description=description, beginsOn=beginsOn, endsOn=endsOn, location=location, onlineAddress=url)
            if picture_id is not None :
                params["picture"] = {}
                params["picture"]["media_id"] = picture_id
            result = self.graphql_client.execute(query=self.query_create_event, variables=params)
            print("KeskonfeIndexer | create_event | Event "+title+" created")
            self.existing_events.append({"title": title, "beginsOn": beginsOn, "url": url})
        except Exception as e: 
            print("KeskonfeIndexer | create_event | Creation event failed :")
            print(e)

    def run(self):
        pass
# Keskonfe Indexer

Command Line tool to index events from some sources (ics file, facebook events etc) and add this to Mobilizon Instance. This tool was developed for the Keskonfé? project. 

## Requirements 
- python 3.10
- chromium 

## Getting Started 

```
python -m venv env
source env/bin/activate

pip install -r requirements.txt

python index.py --place <placename>
```


import json

from keskonfe_indexer import KeskonfeIndexer
from utils.driver import setup_driver
from utils.locale import facebook_www_to_locale
from utils.scrape_and_parse import scrape_events
from selenium.webdriver.common.by import By



class FacebookIndexer(KeskonfeIndexer):
    
    url_calendar = ""
    config_file = ""
    calendar = []
    
    def __init__(self, url_calendar, config_file, organizer_actor_id):
        self.url_calendar = url_calendar
        self.config_file = config_file
        self.organizer_actor_id = organizer_actor_id
        super().__init__()

    def get_calendar(self, existing_events_url):
        driver = setup_driver(headless=True)
        self.calendar = scrape_events(driver, facebook_www_to_locale(self.url_calendar), existing_events_url)
        driver.quit()


    def read_access_token(self):
        with open(self.config_file) as json_file:
            data = json.load(json_file)
            self.access_token = data["access_token"]
            self.refresh_token = data["refresh_token"]

    def save_new_token(self, new_access_token, new_refresh_token):
        data = {}
        with open(self.config_file) as json_file:
            data = json.load(json_file)
            data["access_token"] = new_access_token
            data["refresh_token"] = new_refresh_token
            self.access_token = new_access_token
            self.refresh_token = new_refresh_token
        data_to_write = json.dumps(data, indent=4)
        # Writing to sample.json
        with open(self.config_file, 'w') as outfile:
            outfile.write(data_to_write)

    def run(self):
        self.get_events()
        existing_events_url = []
        for event in self.existing_events:
            if "onlineAddress" in event:
                existing_events_url.append(event.onlineAddress)
        self.get_calendar(existing_events_url)
        for event in self.calendar:
            picture_id = None
            if not self.event_already_exist(event.name, str(event.datetime.strftime("%Y-%m-%dT%H:%M:%SZ")), event.url):
                picture_id = self.upload_picture_from_url(event.picture_url)
                location = self.search_existing_address(event.location, event.geoloc)
                self.create_event(picture_id=picture_id, title=event.name, description=event.description,
                                beginsOn=str(event.datetime.strftime("%Y-%m-%dT%H:%M:%SZ")), location=location, url=event.url)

#!/usr/bin/env python3

import urllib.request
import icalendar

import json 

from keskonfe_indexer import KeskonfeIndexer

class AstronefIndexer(KeskonfeIndexer):
  url_calendar = "http://cafe-lastronef.fr/programme/liste/?tribe_paged=1&tribe_page_size=50&tribe_event_display=list&tribe-bar-date=2023-10-27&ical=1&tribe_display=list"
  calendar = []

  def get_calendar(self):
    urllib.request.urlretrieve(self.url_calendar, "astronef.ics")
    with open("astronef.ics") as f:
      self.calendar = icalendar.Calendar.from_ical(f.read())

  def read_access_token(self):
    with open('indexers/astronef/astronef.config') as json_file:
      data = json.load(json_file)
      self.access_token = data["access_token"]
      self.refresh_token = data["refresh_token"]
  
  def save_new_token(self, new_access_token, new_refresh_token):
    data = {}
    with open('indexers/astronef/astronef.config') as json_file:
      data = json.load(json_file)
      data["access_token"] = new_access_token
      data["refresh_token"] = new_refresh_token
      self.access_token = new_access_token
      self.refresh_token = new_refresh_token
    data_to_write = json.dumps(data, indent=4)
    # Writing to sample.json
    with open('indexers/astronef/astronef.config', 'w') as outfile:
      outfile.write(data_to_write)

  def get_physical_address(self):
    return {
        "country": "France",
        "description": "L'Astronef",
        "geom": "1.4601526;43.5769806",
        "id": None,
        "locality": "Toulouse",
        "originId": "nominatim:5614820521",
        "postalCode": "31400",
        "region": "Occitanie",
        "street": "3 Place des Avions",
        "timezone": "Europe/Paris",
        "type": "house",
        "url": None
    }
  
  def get_organizer_actor_id(self):
    return "5"

  def run(self):
    self.get_calendar()
    self.get_events()
    for event in self.calendar.walk('VEVENT'):
      picture_id = None
      if not self.event_already_exist(event.get("SUMMARY"), str(event['DTSTART'].dt.strftime("%Y-%m-%dT%H:%M:%SZ"))):
        picture_id = self.upload_picture_from_url(event.get("ATTACH"))
        self.create_event(picture_id=picture_id, title=event.get("SUMMARY"), description=event.get("DESCRIPTION"), beginsOn=str(event['DTSTART'].dt.strftime("%Y-%m-%dT%H:%M:%SZ")), endsOn=str(event['DTEND'].dt.strftime("%Y-%m-%dT%H:%M:%SZ")))
    



    

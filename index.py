from indexers.astronef.astronef import AstronefIndexer
from indexers.facebook_indexer import FacebookIndexer

import sys
import json


def str_to_class(classname):
    return getattr(sys.modules[__name__], classname)


with open("indexers.config") as json_file:
    indexers = json.load(json_file)
    for indexer in indexers:
        generated_indexer = str_to_class(indexer["indexer"])(url_calendar=indexer["url_calendar"], config_file=indexer["config_file"], organizer_actor_id=indexer["organizer_actor_id"])
        generated_indexer.run()


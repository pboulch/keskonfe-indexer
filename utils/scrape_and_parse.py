# Built-in imports
from time import sleep

import urllib.request
import json

from selenium.common.exceptions import NoSuchElementException
# Package imports
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys 
from dotenv import load_dotenv
from pathlib import Path
import os

import random

from .Event import Event
from .driver import setup_driver
from .locale import facebook_www_to_locale
# Local imports
from .regex import regex_in, re_utc_and_more, clean_div, clean_emoji

load_dotenv(dotenv_path=Path('./..'))


""" PARSING FUNCTIONS """
def parse_page(driver, existing_events_url):
    source = driver.find_element(By.XPATH, "//h1").text
    event_container = driver.find_element(By.XPATH, """//div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div/div[4]/div/div/div/div/div/div/div/div/div[3]""")
    raw_events = event_container.find_elements(By.XPATH, "*")
    events = []
    for event in raw_events:
        print("KeskonfeIndexer | parge_page | Detected upcoming event in {}!".format("page"))
        raw_data = event.text
        if regex_in(raw_data, re_utc_and_more): # Events with multiple times have multiple entries + one main. This is the main, so skip it
            print("detected main event with times, skipping current")
            continue
        lines = ""
        try:
            lines = raw_data.split("\n")
            name = lines[1]
            url = ""
        except Exception as e:
            print("KeskonfeIndexer | parge_page | Failed to add event from page. No event found ?")
            print(e)
            continue
            
        try:
            # find_element doesn't work, perhaps due to grandchild?
            urls = event.find_elements(By.TAG_NAME, "a")
            url = urls[0].get_attribute("href")
        except Exception:
            print("KeskonfeIndexer | parge_page | Failed to find url events.")
        if not url.split("?")[0] in existing_events_url:
            print("KeskonfeIndexer | parge_page | Parse event ("+url+")")
            (location, image_url, description, geoloc, datetime) = parse_event(driver, url)

            event = Event(name, datetime, source, location, url, geoloc)
            if description:
                print("KeskonfeIndexer | parge_page | Add description")
                event.description = description
            if image_url:
                print("KeskonfeIndexer | parge_page | Add image URL")
                event.picture_url = image_url
            events.append(event)
    return events

def find_json_in_string(data_string: str, key: str, is_desired_value=None):
    prefix = '"'+key+'":'
    start_position = 0

    while True:
        idx = data_string.find(prefix, start_position)
        if idx == -1:
            return {'startIndex': -1, 'endIndex': -1, 'jsonData': None}

        idx += len(prefix)
        start_index = idx
        start_character = data_string[start_index]

        # Value is null
        if start_character == 'n' and data_string[start_index:start_index + 4] == 'null':
            return {'startIndex': start_index, 'endIndex': start_index + 3, 'jsonData': None}

        # Unexpected start character
        if start_character not in ('{', '['):
            raise ValueError(f'Invalid start character: {start_character}')

        end_character = '}' if start_character == '{' else ']'
        nested_level = 0
        is_index_in_string = False

        while idx < len(data_string) - 1:
            idx += 1

            if data_string[idx] == '"' and data_string[idx - 1] != '\\':
                is_index_in_string = not is_index_in_string
            elif data_string[idx] == end_character and not is_index_in_string:
                if nested_level == 0:
                    break
                nested_level -= 1
            elif data_string[idx] == start_character and not is_index_in_string:
                nested_level += 1

        json_data_string = data_string[start_index:idx + 1]
        json_data = json.loads(json_data_string)

        if is_desired_value is None or is_desired_value(json_data):
            return {'startIndex': start_index, 'endIndex': idx, 'jsonData': json_data}

        start_position = idx

def is_location(json_data):
    return "one_line_address" in json_data and "event_description" in json_data

def is_event_with_date(json_data):
    return "start_timestamp" in json_data

# Parse the event page in a secondary driver. This is universal for pages & communities
def parse_event(driver, event_url):
    location = None
    image_url = None
    description = None
    event_url = facebook_www_to_locale(event_url)
    geoloc = None
    datetime = None

    try:
        print("Searching for location & image in " + event_url)
        original_window = driver.current_window_handle
        driver.switch_to.new_window('tab')
        driver.get(event_url)
        event = find_json_in_string(driver.page_source, key= "event", is_desired_value=is_location)["jsonData"]
        cover = find_json_in_string(driver.page_source, key= "cover_photo")["jsonData"]
        event_with_date = find_json_in_string(driver.page_source, key="event", is_desired_value=is_event_with_date)["jsonData"]
    
        driver.execute_script(f"window.scrollTo(0, { random.randint(500,1080) });")

        if "one_line_address" in event and event["one_line_address"] != None:
            location = event["one_line_address"]
        elif "event_place" in event and "contextual_name" in event["event_place"] and "location" in event["event_place"] and "latitude" in event["event_place"]["location"] and "longitude" in event["event_place"]["location"]:
            location = event["event_place"]["contextual_name"]
            geoloc = {
                "longitude": str(event["event_place"]["location"]["longitude"]),
                "latitude": str(event["event_place"]["location"]["latitude"])
            }
        else:
            print("KeskonfeIndexer | parse_event | address not found ("+event_url+")")
        
        if "event_description" in event and "text" in event["event_description"]:
            description = event["event_description"]["text"]

        if "start_timestamp" in event_with_date:
            datetime = event_with_date["start_timestamp"]
        
        else:
            print("KeskonfeIndexer | parse_event | event_description not found ("+event_url+")")

        if "photo" in cover and "full_image" in cover["photo"] and "uri" in cover["photo"]["full_image"]:
            image_url = cover["photo"]["full_image"]["uri"]
        else:
            print("KeskonfeIndexer | parse_event | image_url not found ("+event_url+")")
        sleep(random.randint(1, 9))
                
    except Exception as e:
        print("KeskonfeIndexer | parse_event | Exception : ")
        print(e)
    
    driver.close()
    driver.switch_to.window(original_window)
    return (location, image_url, description, geoloc, datetime)

def facebook_login(driver):

    cookie_element = driver.find_element(By.CSS_SELECTOR, "[aria-label='Decline optional cookies']")
    actions = ActionChains(driver)
    actions.move_to_element(cookie_element)
    actions.click(cookie_element)
    actions.perform()
    sleep(random.randint(1,3))

    email_field = driver.find_element(By.CSS_SELECTOR, "[aria-label='Email address or phone number']")
    email_field.click()
    email_field.send_keys(os.getenv("KESKONFE_FACEBOOK_USERNAME"))
    sleep(random.randint(1,3))

    password_field = driver.find_element(By.CSS_SELECTOR, "[aria-label='Password']")
    password_field.click()
    password_field.send_keys(os.getenv("KESKONFE_FACEBOOK_PASSWORD"))
    sleep(random.randint(1,3))

    login_button = driver.find_element(By.ID, "loginbutton")
    login_button.click()


def scrape_events(driver, url, existing_events_url):
    """ RUNNING PARSE FUNCTIONS ON PAGES """

    events = []
    driver.get(url)
    sleep(5)
    try:
        facebook_login(driver)
        sleep(15)
    except:
        pass
    try:
        close_modale = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[1]/div/div[5]/div/div/div[1]/div/div[2]/div/div/div/div[1]/div/i")
        driver.execute_script("arguments[0].click();", close_modale)
        sleep(1)
    except:
        pass

    driver.execute_script("window.scrollTo(0, 1080);")
    sleep(5)

    try:
        events.extend(parse_page(driver, existing_events_url=existing_events_url))
    except NoSuchElementException as e:
        print("Error parsing {}".format(url))
        print("No events found.")
        print(e)

    except Exception as e:
        print("Error parsing {}".format(url))
        raise e

    return events

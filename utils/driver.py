# Built-in imports
from platform import system, machine

# Package imports
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.proxy import Proxy, ProxyType
import requests
import random
#from pyvirtualdisplay import Display

def get_proxy(): 
    #Get list of proxies
    response = requests.get("https://proxylist.geonode.com/api/proxy-list?anonymityLevel=elite&protocols=http&limit=500&page=1&sort_by=lastChecked&sort_type=desc")
    list_of_proxies = response.json()
    #list_of_proxies_http = [x for x in list_of_proxies["proxies"] if x['protocol'] == 'http']
    proxy_http = random.choice(list_of_proxies['data'])
    selenium_proxy = Proxy()
    selenium_proxy.proxy_type = ProxyType.MANUAL
    selenium_proxy.http_proxy = ""#proxy_http['proxy']
    print(f"KeskonfeIndexer | get_proxy | Select proxy: {proxy_http['ip']}:{proxy_http['port']}")
    #return selenium_proxy
    return f"http://{proxy_http['ip']}:{proxy_http['port']}"

# Setup Driver
def setup_driver(headless=True):
    options = webdriver.ChromeOptions()

    if (headless):
        headless_opts = [
            "--headless=new",
            "--disable-gpu",
            "--window-size=1920,1200",
            "---ignore-certificate-errors",
            "--disable-extensions",
            "--no-sandbox",
            "--disable-dev-shm-usage",
            #"--remote-debugging-port=9515",
            "--disable-setuid-sandbox"
        ]
        for opt in headless_opts:
            options.add_argument(opt)
            #print(opt)
    
    # Much thanks to https://stackoverflow.com/a/71042821
    raspberry_pi = system() == "Linux" and machine() == "armv7l"
    if not raspberry_pi:
        service=ChromeService(ChromeDriverManager().install())
    else:
        #display = Display(visible=0, size=(1920,1200))
        #display.start()
        raspbian_chromium = "/usr/lib/chromium-browser/chromedriver"
        service = ChromeService(raspbian_chromium)
        #options.binary_location = raspbian_chromium

    #options.proxy = get_proxy()
    #options.add_argument(f"--proxy-server={get_proxy()}")
    return webdriver.Chrome(service=service, options=options)